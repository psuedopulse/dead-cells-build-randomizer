const items = require("./items.js");

let stat;
let weapon1;
let weapon2;
let skill1;
let skill2;
let mut1;
let mut2;
let mut3;
let backpack;

const opt = require("yargs")
  .scriptName('dcbuildrandom')
  .usage('dcbuildrandom --stat [stat] --weapon [weapon]')
  .command('generate [stat] [weapon]', 'Generates a random build with the specified options', (yargs) => {
    yargs.option('stat', {
      alias: 's',
      default: 'random',
      describe: 'Choose a build using this stat'
    });

    yargs.option('weapon', {
      alias: 'w',
      default: 'random',
      describe: 'Choose a build using this weapon'
    });
  }, function (argv) {
    //stat
    if (argv.stat.toLowerCase() == "brutality") stat = 1;
    if (argv.stat.toLowerCase() == "survival") stat = 2;
    if (argv.stat.toLowerCase() == "tactics") stat = 3;

    //weapon
    if (argv.weapon != "random") {
      //strip all non alphabet chars when searching
      let regex = /[a-z]/g;
      let filteredItems = items.weapons.filter(item => item.name.toLowerCase().match(regex).join("").includes(argv.weapon.toLowerCase().match(regex).join("")));

      //pick weapon
      weapon1 = filteredItems[Math.floor(Math.random() * filteredItems.length)]
      if (weapon1 == undefined) throw new Error(`Couldn't find ${argv.weapon} in the list of weapons!`);

      //make sure stat matches the weapon
      if (stat == undefined) stat = weapon1.stat;
      if (stat == 12) stat = Math.random() < 0.5 ? 1 : 2;
      if (stat == 13) stat = Math.random() < 0.5 ? 1 : 3;
      if (stat == 23) stat = Math.random() < 0.5 ? 2 : 3;
    }
  })
  .help()
  .argv;

function hasSameStat(item) {
  if (stat == 1) return item.stat == 1 || item.stat == 12 || item.stat == 13 || item.stat == 0;
  if (stat == 2) return item.stat == 2 || item.stat == 12 || item.stat == 23 || item.stat == 0;
  if (stat == 3) return item.stat == 3 || item.stat == 13 || item.stat == 23 || item.stat == 0;
  return false;
}

function hasStatusSynergy(item, compareTo) {
  switch (compareTo.status) {
    case 1: //bleeding
      return (item.status == 1 || item.status == -1) && item.status != 5;
    case 2: //poison
      return (item.status == 2 || item.status == -1) && item.status != 5;
    case 3: //shock
      return item.status == 3 && item.status != 5;
    case 4: //fire
      return item.status == 9 && item.status != 5;
    case 5: //freeze
      return (item.status == 5 || item.status == -2 || (typeof item == items.Weapon && item.speed == 1)) &&
        item.status < 1 && item.status > 4 && item.status != -4 && item.status != -1;
    case 6: //root
      return item.status == 6 || item.status == -2 || (typeof item == items.Weapon && item.speed == 1);
    case 7: //slow
      return (typeof item == items.Weapon && item.speed == 1);
    case 8: //stun
      return item.status == 8 || item.status == -2 || (typeof item == items.Weapon && item.speed == 1);
    case 9: //oil
      return (item.status == 4 || item.status == 9) && item.status != 5;
    case -1: //stiletto
      return (item.status == 1 || item.status == 2) && item.status != 5;
    case -2: //nutcracker
      return item.status == 5 || item.status == 6 || item.status == 8;
    case -3: //hokuto
      return item.status >= 1 && item.status <= 4;
    case -4: //swarm
      return item.status != 5;
  }
  if (typeof compareTo == compareTo.Weapon && compareTo.speed == 1) return item.status >= 5 || item.status <= 8;
  return false;
}

function hasNoAntiSynergy(item, compareTo) {
  if (compareTo.status == 5) {
    return (item.status < 1 || item.status > 4) && item.status != -4 && item.status != -1;
  }
  if ((compareTo.status >= 1 && compareTo.status <= 4) || compareTo.status == -4 || compareTo.status == -1) {
    return item.status != 5;
  }
  return true;
}

function hasCC(item) {
  return item.status == 5 || item.status == 6 || item.status == 7 || item.status == 8;
}

function pickRandomFromStat(items) {
  let filteredItems = items.filter(hasSameStat);
  return filteredItems[Math.floor(Math.random() * filteredItems.length)];
}


function generateBP(type) { // type should be whatever mutation is the backpack one
  switch (type) {
    case 1: // Porcupack
      backpack = pickRandomFromStat(items.weapons.filter((item) => item.melee && hasStatusSynergy(item, weapon1)));
      if (backpack == undefined) backpack = pickRandomFromStat(items.weapons.filter((item) => item.melee && hasNoAntiSynergy(item, weapon1)));
      break;
    case 2: // Armadillopack
      backpack = pickRandomFromStat(items.weapons.filter((item) => item.shield));
      break;
    case 3: // Acrobatipack
      backpack = pickRandomFromStat(items.weapons.filter((item) => item.name == "Lightning Bolt" || item.name == "Hokuto's Bow" || item.name == "Alchemic Carbine" || item.name == "Gilded Yumi" || item.name == "Fire Blast" || item.name == "Frost Blast"));
      break;
    default:
      return 0; // this should never happen
  }
};


//-------------
//--pick stat--
//-------------

if (stat == undefined) stat = Math.floor(Math.random() * 3) + 1;

//----------------
//--pick weapons--
//----------------

//weapon 1
if (weapon1 == undefined) weapon1 = pickRandomFromStat(items.weapons.filter(item => !item.shield));
//weapon 2
if (weapon1.support) { //needs a main weapon that synergizes
  let filteredItems = items.weapons.filter(item => !(item.dual || item.shield || item.support));
  weapon2 = pickRandomFromStat(filteredItems.filter(item => hasStatusSynergy(item, weapon1)));
  if (weapon2 == undefined) weapon2 = pickRandomFromStat(filteredItems.filter(item => hasNoAntiSynergy(item, weapon1) && item.melee == weapon1.melee ? true : false));

  //swap the weapons so the support is in secondary slot
  let weaponTemp = weapon1;
  weapon1 = weapon2;
  weapon2 = weaponTemp;
}
else if (!weapon1.dual) { //get this man a shield!
  let filteredItems = items.weapons.filter(item => item.shield || item.support && hasStatusSynergy(item, weapon1));
  if (weapon1.status != 0) weapon2 = pickRandomFromStat(filteredItems.filter(item => hasStatusSynergy(item, weapon1)));
  if (weapon2 == undefined) weapon2 = pickRandomFromStat(filteredItems.filter(item => hasNoAntiSynergy(item, weapon1)));
}
if (weapon1.dual) weapon2 = weapon1;

//---------------
//--pick skills--
//---------------

//skill 1 - should be cc
let filteredItems;

if (weapon1.status != 0) {
  filteredItems = items.skills.filter(item => hasStatusSynergy(item, weapon1));
}
else {
  filteredItems = items.skills.filter(item => hasNoAntiSynergy(item, weapon1));
}
skill1 = pickRandomFromStat(filteredItems.filter(item => item.cc));

if (skill1 == undefined) {
  skill1 = pickRandomFromStat(items.skills.filter(item => hasNoAntiSynergy(item, weapon1) && item.cc));
}

//if the primary weapon had no status, start searching for synergies with this skill
if (weapon1.status == 0 && skill1.status != 0) filteredItems = items.skills.filter(item => hasStatusSynergy(item, skill1));
//make sure skill 2 isn't same as skill 1
filteredItems = filteredItems.filter(item => item != skill1);

//skill 2
if (weapon1.name == "Dagger of Cupidity" || weapon1.name == "Spite Sword") {
  skill2 = pickRandomFromStat(items.skills.filter(item => item.name == "Face Flask"));
}
else if (weapon1.name == "Sadist's Stiletto" || weapon1.name == "Assassin's Dagger" || weapon1.name == "Infantry Bow") {
  skill2 = pickRandomFromStat(items.skills.filter(item => item.name == "Phaser"));
}
else if (weapon1.melee && weapon1.speed != 1) { //fast melee - should be dps/utility
  skill2 = pickRandomFromStat(filteredItems.filter(item => item.dps));
  if (skill2 == undefined) skill2 = pickRandomFromStat(filteredItems.filter(item => item.utility));
}
else if (weapon1.melee) { //slow melee - should be utility
  skill2 = pickRandomFromStat(filteredItems.filter(item => item.utility));
}
else if (weapon1.ranged) { //ranged - should be dps
  skill2 = pickRandomFromStat(filteredItems.filter(item => item.dps));
}
if (skill2 == undefined) {
  skill2 = pickRandomFromStat(items.skills.filter(item => hasNoAntiSynergy(item, skill1) && item.type != skill1.type));
}

//------------------
//--pick mutations--
//------------------

//brutality
if (stat == 1) {
  //mut 1 is general biome clearing or special
  if (weapon1.speed == 1) {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Melee"));
  }
  else if (weapon1.name == "Swift Sword") {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Velocity"));
  }
  else if (weapon1.name == "Dagger of Cupidity") {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Midas' Blood"));
  }
  else if (weapon1.speed == 2) {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.type == 3));
  }
  else {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.type == 1));
  }

  //mut 2 is damage buff or skill
  if (skill1.name == "Face Flask" || skill2.name == "Face Flask") {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.name == "Vengeance"));
  }
  else if (!weapon1.dual && (weapon1.status == 1 || weapon1.status == -1 || weapon2.status == 1 || skill1.status == 1 || skill2.status == 1)) {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.name == "Open Wounds"));
  }
  else {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.type == 2 || item.type == 4));
  }

  //mut 3 is survivability
  mut3 = pickRandomFromStat(items.mutations.filter(item => item.type == 5));
}

//survival
if (stat == 2) {
  //mut 1 is skill or kill rhythm
  if (hasCC(weapon1) || hasCC(weapon2) && weapon1.speed < 1) { // weapon has cc and is faster than 1
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Heart of Ice"));
  }
  else if ((skill1.cd + skill2.cd > 15) && weapon2.shield) {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Blind Faith"));
  }
  else if (weapon2.shield) {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Counterattack" || item.name == "Spite"));
  }
  else if ((weapon1.dual || !weapon2.shield) && weapon1.name != "Repeater Crossbow|Quiver of Bolts") {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Kill Rhythm"));
  }
  else if (skill1.cd + skill2.cd > 15) {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Instinct of the Master of Arms"));
  }
  else mut1 = pickRandomFromStat(items.mutations)

  //mut 2 is point blank or survivability
  if (weapon1.ranged) { // if weapon is ranged give off-color pb
    mut2 = items.mutations.filter(item => item.name == "Point Blank")[0];
  }
  else { // if melee give tanky stuff
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.name == "Berserker" || item.name == "Soldier's Resistance"));
  }

  //mut 3 is survivability
  if (weapon1.dual || !weapon2.shield) {
    mut3 = pickRandomFromStat(items.mutations.filter(item => item.name == "Armadillopack"));
  }
  else if (!weapon2.shield) {
    mut3 = pickRandomFromStat(items.mutations.filter(item => item.type == 5 && item != mut1 && item != mut2 && item.name != "What Doesn't Kill Me"));
  }
  else {
    mut3 = pickRandomFromStat(items.mutations.filter(item => item.type == 5 && item != mut1 && item != mut2));
  }
}

//tactics
if (stat == 3) {
  //mut 1 is damage buff or skill
  if (weapon1.ranged && weapon1.longRange) {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Tranquility"));
  }
  else if (weapon1.ranged && !weapon1.longRange) {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Point Blank"));
  }
  else if (weapon1.melee) {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.name == "Support"));
  }
  else {
    mut1 = pickRandomFromStat(items.mutations.filter(item => item.type == 4));
  }

  //mut 2 is arrow related, crow's foot, single hit buff, or skill
  if (weapon1.name == "The Boy's Axe" || weapon1.name == "Ferryman's Lantern|Soul Shot") {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.name == "Ammo"));
  }
  else if (weapon1.ammo && weapon1.speed == 3) {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.name == "Barbed Tips"));
  }
  else if (weapon1.ammo) {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.type == 6 && item.name != "Barbed Tips"));
  }
  else if (weapon1.melee) {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.name == "Crow's Foot"));
  }
  else if (weapon1.speed == 2) {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.type == 3));
  }
  else {
    mut2 = pickRandomFromStat(items.mutations.filter(item => item.type == 4 && item != mut1));
  }

  //mut 3 is survivability
  if (weapon2.name == "Money Shooter") {
    mut3 = pickRandomFromStat(items.mutations.filter(item => item.type == 5 && item.name != "Gold Plating")); // don't assign gold plating to money shooter
  }
  else {
    mut3 = pickRandomFromStat(items.mutations.filter(item => item.type == 5 && item != mut1 && item != mut2));
  }
}

if (mut1 == undefined) throw new Error("Mutation #1 could not be selected");
if (mut2 == undefined) throw new Error("Mutation #2 could not be selected");
if (mut3 == undefined) throw new Error("Mutation #3 could not be selected");

//------------
//--backpack--
//------------

if (mut1.name == "Porcupack" || mut2.name == "Porcupack" || mut3.name == "Porcupack") generateBP(1);
if (mut1.name == "Armadillopack" || mut2.name == "Armadillopack" || mut3.name == "Armadillopack") generateBP(2);
if (mut1.name == "Acrobatipack" || mut2.name == "Acrobatipack" || mut3.name == "Acrobatipack") generateBP(3);

//---------------
//--print build--
//---------------

let primary;
let secondary;
let statStr;

if (weapon1.dual) {
  primary = weapon1.name.split("|")[0];
  secondary = weapon1.name.split("|")[1];
}
else {
  primary = weapon1.name;
  secondary = weapon2.name;
}

if (stat == 1) statStr = "Brutality";
if (stat == 2) statStr = "Survival";
if (stat == 3) statStr = "Tactics";


console.log("-- Your Build Is --");
console.log("Primary:", primary);
console.log("Secondary:", secondary);
console.log("Backpack:", backpack != undefined ? backpack.name : "None");
console.log("Skills:", skill1.name, "and", skill2.name);
console.log("Mutations:", mut1.name + ",", mut2.name + ", and", mut3.name);
console.log("Stat:", statStr);

// write to json for integration
var fs = require('fs');

try {
  fs.unlinkSync('./build.json'); // remove previous output file
} catch {
  console.log(`Couldn't remove build.json`);
};

let build = {
  "Primary": primary,
  "Secondary": secondary,
  "Backpack": backpack != undefined ? backpack.name : "None",
  "Skills": skill1.name + " and " + skill2.name,
  "Mutations": mut1.name + ", " + mut2.name + ", and " + mut3.name,
  "Stat": statStr
}

build = JSON.stringify(build);
fs.writeFile('./build.json', build, 'utf8', (err) => {
  if (err) {
    throw new Error(`Couldn't write to build.json:`, err);
  }
});
