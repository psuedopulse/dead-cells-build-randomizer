//weapons
function Weapon(name, stat, dual, shield, ranged, melee, ammo, longRange, speed, support, status) {
  this.name = name; // string: weapon's name
  this.stat = stat; // integer: weapon's color; 1 - brutality, 2 - survival, 3 - tactics, combine for dual scale (eg; iron staff would be 12)
  this.dual = dual; // boolean: is it two-handed?
  this.shield = shield; // boolean: is it a shield?
  this.ranged = ranged; // boolean: is it a ranged weapon?
  this.melee = melee; // boolean: is it a melee weapon?
  this.ammo = ammo; // boolean: is it affected by the ammo mutation?
  this.longRange = longRange; // boolean: is it a long-distance weapon (eg; nerves of steel or marksman's bow)
  this.speed = speed; // integer: how fast is it/how does the weapon behave? 0 - shield, 1 - slow/big hit, 2 - medium/big hit, 3 - autofire/small hits
  this.support = support; // boolean: is it a support weapon? (eg; frost blast)
  this.status = status; // integer: does this inflict a status or crit on a status?
  // 0 = no status; 1 = bleed; 2 = poison; 3 = shock; 4 = fire; 5 = freeze; 6 = root; 7 = slow; 8 = stun; 9 = oil
  // -1 = this item is sadist's stiletto; -2 = this item is nutcracker, -3 = this item is hokuto's bow, -4 = this item is swarm
};

const weapons = [];

//4482423842 generic melee weapons
weapons.push(new Weapon("Balanced Blade", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Assassin's Dagger", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Spite Sword", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Blood Sword", 1, false, false, false, true, false, false, 3, false, 1));
weapons.push(new Weapon("Twin Daggers", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Broadsword", 2, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Shovel", 2, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Cursed Sword", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Sadist's Stiletto", 13, false, false, false, true, false, false, 2, false, -1));
weapons.push(new Weapon("Swift Sword", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Giantkiller", 12, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Shrapnel Axes", 13, false, false, true, true, true, false, 2, false, 0));
weapons.push(new Weapon("Seismic Strike", 2, false, false, false, true, false, false, 1, true, 6));
weapons.push(new Weapon("War Spear", 12, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Impaler", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Symmetrical Lance", 2, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Rapier", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Meat Skewer", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Nutcracker", 2, false, false, false, true, false, false, 1, false, -2));
weapons.push(new Weapon("Spartan Sandals", 13, false, false, false, true, false, false, 3, true, 0));
weapons.push(new Weapon("Spiked Boots", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Hayabusa Boots", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Hayabusa Gauntlets", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Valmont's Whip", 13, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Wrenching Whip", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Oiled Sword", 1, false, false, false, true, false, false, 3, false, 9));
weapons.push(new Weapon("Torch", 1, false, false, false, true, false, false, 3, true, 4));
weapons.push(new Weapon("Frantic Sword", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Flawless", 12, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Flint", 12, false, false, false, true, false, false, 1, false, 4));
weapons.push(new Weapon("Tentacle", 13, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Vorpan", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Scythe Claw|Left Scythe Claw", 2, true, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Rhythm n' Bouzouki", 2, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Crowbar", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Snake Fangs", 13, false, false, false, true, false, false, 3, false, 2));
weapons.push(new Weapon("Iron Staff", 12, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Ferryman's Lantern|Soul Shot", 13, true, false, true, true, true, false, 2, false, 0));
weapons.push(new Weapon("Hattori's Katana", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Tombstone", 2, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Oven Axe", 2, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Toothpick", 2, false, false, false, true, false, false, 1, false, 8));
weapons.push(new Weapon("Machete and Pistol", 13, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Hard Light Sword|Hard Light Gun", 13, true, false, true, true, true, false, 2, false, 0));
weapons.push(new Weapon("Pure Nail", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Bone", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Abyssal Trident", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Hand Hook", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Maw of the Deep", 2, false, false, false, true, false, false, 1, false, 6));
weapons.push(new Weapon("Bladed Tonfas", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Wrecking Ball", 2, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Queen's Rapier", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Dagger of Cupidity", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Gold Digger", 2, false, false, false, true, false, false, 1, false, 0));

//ranged weapons
weapons.push(new Weapon("Multiple-Knocks Bow", 3, false, false, true, false, true, true, 3, false, 0));
weapons.push(new Weapon("Bow and Endless Quiver", 3, false, false, true, false, false, true, 3, false, 0));
weapons.push(new Weapon("Marksman's Bow", 3, false, false, true, false, true, true, 1, false, 0));
weapons.push(new Weapon("Sonic Carbine", 3, false, false, true, false, true, true, 3, false, 0));
weapons.push(new Weapon("Infantry Bow", 13, false, false, true, false, true, false, 2, false, 0));
weapons.push(new Weapon("Quick Bow", 3, false, false, true, false, true, false, 3, false, 0));
weapons.push(new Weapon("Ice Bow", 23, false, false, true, false, true, false, 1, true, 5));
weapons.push(new Weapon("Heavy Crossbow|Reload", 23, true, false, true, false, true, false, 1, false, 6));
weapons.push(new Weapon("Repeater Crossbow|Quiver of Bolts", 23, true, false, true, false, true, true, 3, false, 6));
weapons.push(new Weapon("Ice Crossbow|Piercing Shot", 23, true, false, true, false, true, true, 2, false, 5));
weapons.push(new Weapon("Explosive Crossbow|Cross Hit", 23, true, false, true, true, true, false, 1, false, 0));
weapons.push(new Weapon("Alchemic Carbine", 3, false, false, true, false, false, true, 3, true, 2));
weapons.push(new Weapon("Boomerang", 3, false, false, true, false, false, false, 2, true, 4));
weapons.push(new Weapon("Hemorrhage", 13, false, false, true, false, false, false, 1, false, 1));
weapons.push(new Weapon("The Boy's Axe", 23, false, false, true, false, true, false, 3, false, 6));
weapons.push(new Weapon("War Javelin", 3, false, false, true, false, false, false, 2, true, 0));
weapons.push(new Weapon("Hokuto's Bow", 3, false, false, true, false, false, true, 3, true, -3));
weapons.push(new Weapon("Nerves of Steel", 3, false, false, true, false, true, true, 2, false, 0));
weapons.push(new Weapon("Throwing Knife", 13, false, false, true, false, true, true, 3, true, 1));
weapons.push(new Weapon("Electric Whip", 3, false, false, true, false, false, false, 3, false, 3));
weapons.push(new Weapon("Firebrands", 13, false, false, true, false, false, false, 3, true, 4));
weapons.push(new Weapon("Ice Shards", 23, false, false, true, false, false, false, 3, true, 7));
weapons.push(new Weapon("Pyrotechnics", 13, false, false, true, false, false, false, 3, false, 4));
weapons.push(new Weapon("Lightning Bolt", 3, false, false, true, false, false, false, 3, false, 3));
weapons.push(new Weapon("Fire Blast", 13, false, false, true, false, false, false, 3, false, 4));
weapons.push(new Weapon("Frost Blast", 23, false, false, true, false, false, false, 1, true, 5));
weapons.push(new Weapon("Magic Missiles", 3, false, false, true, false, false, false, 3, false, 0));
weapons.push(new Weapon("Blowgun", 3, false, false, true, false, true, false, 2, false, 2));
weapons.push(new Weapon("Barrel Launcher", 3, false, false, true, false, false, false, 1, false, 0));
weapons.push(new Weapon("Killing Deck", 3, false, false, true, false, false, false, 2, false, 0));
weapons.push(new Weapon("Gilded Yumi", 3, false, false, true, false, false, false, 1, true, 0));
weapons.push(new Weapon("Money Shooter", 3, false, false, true, false, false, false, 3, true, 0));

//shields
weapons.push(new Weapon("Front Line Shield", 12, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Cudgel", 2, false, true, false, false, false, false, 0, true, 8));
weapons.push(new Weapon("Punishment", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Knockback Shield", 23, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Rampart", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Bloodthirsty Shield", 12, false, true, false, false, false, false, 0, true, 1));
weapons.push(new Weapon("Assault Shield", 12, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Greed Shield", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Spiked Shield", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Parry Shield", 23, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Force Shield", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Thunder Shield", 23, false, true, false, false, false, false, 0, true, 3));
weapons.push(new Weapon("Ice Shield", 2, false, true, false, false, false, false, 0, true, 5));

//skill issue
function Skill(name, stat, cd, status, type, cc, dps, utility) {
  this.name = name; // string: the skill's name
  this.stat = stat; // integer: the stat, numbers to use are at the top of the file
  this.cd = cd; // integer: the skill's cooldown in seconds
  this.status = status; // integer: does this inflict a status? status numbers are at the top of the file
  this.type = type; // integer: 0 = deployable, 1 = grenade, 2 = powers, 3 = pets
  this.cc = cc; // boolean: is this good at crowd control? all builds should have one
  this.dps = dps; // boolean: is this good at just dealing dps? fast melee and ranged should have one
  this.utility = utility; // boolean: is this good for utility, as in movement/survival (tonic, phaser, ice armor)?
  // slow melee builds should have one, fast melee builds should have one if there's nothing else to pick
};

const skills = [];

//deployables
skills.push(new Skill("Double Crossb-o-matic", 3, 10, 0, 0, true, true, false));
skills.push(new Skill("Sinew Slicer", 13, 10, 1, 0, true, true, false));
skills.push(new Skill("Heavy Turret", 3, 10, 0, 0, true, true, false));
skills.push(new Skill("Barnacle", 3, 10, 0, 0, true, true, false));
skills.push(new Skill("Flamethrower Turret", 13, 12, 4, 0, true, true, false));
skills.push(new Skill("Cleaver", 1, 8, 1, 0, true, true, false));
skills.push(new Skill("Wolf Trap", 23, 14, 6, 0, true, false, false));
skills.push(new Skill("Crusher", 23, 14, 7, 0, true, true, false));
skills.push(new Skill("Explosive Decoy", 3, 20, 0, 0, true, true, true));
skills.push(new Skill("Emergency Door", 3, 10, 8, 0, false, false, true));
skills.push(new Skill("Tesla Coil", 3, 10, 3, 0, true, true, false));
skills.push(new Skill("Scavenged Bombard", 23, 14, 0, 0, true, true, false));

//grenades
skills.push(new Skill("Powerful Grenade", 12, 13, 0, 1, false, true, false));
skills.push(new Skill("Infantry Grenade", 1, 4, 0, 1, false, true, false));
skills.push(new Skill("Cluster Grenade", 12, 20, 0, 1, true, true, false));
skills.push(new Skill("Magnetic Grenade", 3, 16, 3, 1, true, true, false));
skills.push(new Skill("Stun Grenade", 2, 12, 8, 1, true, false, true));
skills.push(new Skill("Ice Grenade", 2, 12, 5, 1, true, false, true));
skills.push(new Skill("Fire Grenade", 1, 13, 4, 1, true, true, false));
skills.push(new Skill("Root Grenade", 2, 16, 6, 1, true, false, true));
skills.push(new Skill("Oil Grenade", 13, 10, 9, 1, true, false, false));
skills.push(new Skill("Swarm", 3, 10, -4, 1, true, false, false));

//power skills
skills.push(new Skill("Death Orb", 2, 20, 0, 2, true, true, false));
skills.push(new Skill("Tornado", 3, 18, 0, 2, true, false, false));
skills.push(new Skill("Knife Dance", 13, 16, 1, 2, true, true, false));
skills.push(new Skill("Corrupted Power", 13, 16, 0, 2, false, true, true));
skills.push(new Skill("Vampirism", 12, 30, 0, 2, false, false, true));
skills.push(new Skill("Tonic", 2, 30, 0, 2, false, false, true));
skills.push(new Skill("Grappling Hook", 1, 3, 8, 2, false, false, true));
skills.push(new Skill("Phaser", 13, 2, 0, 2, false, false, true));
skills.push(new Skill("Corrosive Cloud", 13, 20, 1, 2, true, false, false));
skills.push(new Skill("Lacerating Aura", 13, 12, 0, 2, true, true, false));
skills.push(new Skill("Wave of Denial", 3, 5, 8, 2, true, false, false));
skills.push(new Skill("Wings of the Crow", 3, 15, 3, 2, true, true, true));
skills.push(new Skill("Great Owl of War", 3, 10, 0, 3, true, true, false));
skills.push(new Skill("Lightspeed", 13, 10, 0, 2, true, false, false));
skills.push(new Skill("Giant Whistle", 2, 20, 0, 2, true, true, false));
skills.push(new Skill("Telluric Shock", 12, 10, 0, 2, true, false, false));
skills.push(new Skill("||Collector's Syringe||", 12, 20, 0, 2, true, true, false));
skills.push(new Skill("Smoke Bomb", 13, 16, 0, 2, true, true, true));
skills.push(new Skill("Mushroom Boi!", 2, 25, 8, 3, true, true, false));
skills.push(new Skill("Lightning Rods", 3, 15, 0, 2, true, true, false));
skills.push(new Skill("Ice Armor", 2, 30, 5, 2, false, false, true));
skills.push(new Skill("Scarecrow's Sickles", 23, 10, 0, 2, true, true, false));
skills.push(new Skill("Serenade", 1, 30, 0, 3, true, true, false));
skills.push(new Skill("Cocoon", 23, 12, 0, 2, false, false, true));
skills.push(new Skill("Face Flask", 1, 10, 0, 2, false, false, true));
skills.push(new Skill("Leghugger", 13, 10, 1, 3, true, false, false));

//mutation time
function Mutation(name, stat, type) {
  this.name = name; // string: the mutation's name
  this.stat = stat; // integer: the mutation's stat
  this.type = type; // integer: the mutation's type
  // type values: 0 = special; 1 = general, 2 = damage buff, 3 = single hit buff, 4 = skill issue, 5 = survivability,
  // 6 = arrows, 7 = ranged, 8 = backpack
};

const mutations = [];

//brutality
mutations.push(new Mutation("Killer Instinct", 1, 4));
mutations.push(new Mutation("Combo", 1, 2));
mutations.push(new Mutation("Vengeance", 1, 2));
mutations.push(new Mutation("Melee", 1, 1));
mutations.push(new Mutation("Open Wounds", 1, 2));
mutations.push(new Mutation("Tainted Flask", 1, 2));
mutations.push(new Mutation("Adrenaline", 1, 5));
mutations.push(new Mutation("Frenzy", 1, 5));
mutations.push(new Mutation("Scheme", 1, 3));
mutations.push(new Mutation("Initiative", 1, 3));
mutations.push(new Mutation("Predator", 1, 1));
mutations.push(new Mutation("Porcupack", 1, 8));

//tactics
mutations.push(new Mutation("Support", 3, 2));
mutations.push(new Mutation("Parting Gift", 3, 2));
mutations.push(new Mutation("Tranquility", 3, 2));
mutations.push(new Mutation("Ripper", 3, 6));
mutations.push(new Mutation("Ranger's Gear", 3, 3));
mutations.push(new Mutation("Barbed Tips", 3, 6));
mutations.push(new Mutation("Point Blank", 3, 2));
mutations.push(new Mutation("Networking", 3, 6));
mutations.push(new Mutation("Crow's Foot", 3, 5));
mutations.push(new Mutation("Tactical Retreat", 3, 5));
mutations.push(new Mutation("Hunter's Instinct", 3, 4));
mutations.push(new Mutation("Acrobatipack", 3, 8));

//survival
mutations.push(new Mutation("Soldier's Resistance", 2, 5));
mutations.push(new Mutation("Berserker", 2, 5));
mutations.push(new Mutation("Blind Faith", 2, 4));
mutations.push(new Mutation("Counterattack", 2, 2));
mutations.push(new Mutation("Necromancy", 2, 5));
mutations.push(new Mutation("What Doesn't Kill Me", 2, 5));
mutations.push(new Mutation("Extended Healing", 2, 5));
mutations.push(new Mutation("Spite", 2, 0));
mutations.push(new Mutation("Gastronomy", 2, 5));
mutations.push(new Mutation("Frostbite", 2, 0));
mutations.push(new Mutation("Heart of Ice", 2, 4));
mutations.push(new Mutation("Kill Rhythm", 2, 0));
mutations.push(new Mutation("Armadillopack", 2, 8));

//colorless
//yolo - not included
mutations.push(new Mutation("Recovery", 0, 0));
mutations.push(new Mutation("Emergency Triage", 0, 0));
mutations.push(new Mutation("Velocity", 0, 0));
mutations.push(new Mutation("Dead Inside", 0, 0));
mutations.push(new Mutation("Gold Plating", 0, 5));
mutations.push(new Mutation("Midas' Blood", 0, 0));
mutations.push(new Mutation("Get Rich Quick", 0, 0));
//alienation - not included
//acceptance - not included
//masochist - not included
mutations.push(new Mutation("Disengagement", 0, 5));
mutations.push(new Mutation("No Mercy", 0, 0));
mutations.push(new Mutation("Instinct of the Master of Arms", 0, 0));
mutations.push(new Mutation("Ammo", 0, 6));

module.exports = { weapons, skills, mutations, Weapon, Skill, Mutation };
