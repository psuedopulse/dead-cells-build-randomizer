# Dead Cells Build Randomizer
Selects a random, but viable build for your next Dead Cells run!
* Avoids generating complete nonsense, so you'll never end up needing to run Wrecking Ball + Vampirism or Oven Axe + Adrenaline
* Matches similar items together, and pairs items with others that to complete their synergies! (ex: Sadist's Stiletto + Open Wounds)
* Gives items that match a specific stat or playstyle depending on the primary weapon, so you won't get a Toothpick burst build

# Installation
```
git clone https://gitlab.com/psuedopulse/dead-cells-build-randomizer
cd dead-cells-build-randomizer
npm install
```

# Usage
```
node . generate [parameters]
```

* Arguments -
```
--stat : Optional - only choose builds matching this stat. Valid input: "brutality", "survival", "tactics"
```
```
--weapon : Optional - only choose a build with this weapon. Not case sensitive and ignores punctuation and whitespace
```

* Generates a fully random build if no option is specified

# Dependencies
* Yargs
* Node.JS

