const weaponsMelee = [
    "Seismic Strike",
    "Nutcracker",
    "Shovel",
    "Symmetrical Lance",
    "Flawless",
    "Flint",
    "Iron Staff",
    "Tombstone",
    "Oven Axe",
    "Toothpick",
    "Maw of the Deep",
    "Wrecking Ball"
]

const weaponsRanged = [
    "Ice Bow",
    "The Boy's Axe",
    "Ice Shards",
    "Frost Blast",
]

const weaponsDualwield = [
    "Heavy Crossbow",
    "Repeater Crossbow",
    "Ice Crossbow",
    "Explosive Crossbow",
    "Scythe Claws",
]

const weaponsDOT = [

]

const shields = [
    "Old Wooden Shield",
    "Front Line Shield",
    "Cudgel",
    "Punishment",
    "Knockback Shield",
    "Rampart",
    "Bloodthirsty Shield",
    "Assault Shield",
    "Greed Shield",
    "Spiked Shield",
    "Parry Shield",
    "Force Shield",
    "Thunder Shield",
    "Ice Shield",
]

const skillsTurret = [
    "Scavenged Bombard",
]

const skillsGrenade = [
    "Powerful Grenade",
    "Cluster Grenade",
    "Stun Grenade",
    "Ice Grenade",
    "Root Grenade",
]

const skillsPower = [
    "Tonic",
    "Vampirism",
    "Telluric Shock",
    "Death Orb",
    "Giant Whistle",
    "Collector's Syringe",
    "Mushroom Boi!",
    "Ice Armor",
    "Scarecrow Sickles",
    "Cocoon",
]

const mutationsGeneric = [
    "Soldier's Resistance",
    "Spite",
    "Frostbite",
    "Heart of Ice",
    "Kill Rhythm",
]

const mutationsMelee = [
    "Berserker",
    "Blind Faith",
    "Counterattack",
    "What Doesn't Kill Me",
    "Necromancy",
]

const mutationsRanged = [
    
]

const mutationsDOT = [

]

const mutationsMisc = [
    "Extended Healing",
    "Gastronomy",
    "Ygdar Orus Li Ox",
    "Recovery",
    "Emergency Triage",
    "Velocity",
    "Alienation",
    "Acceptance",
    "Masochist",
    "Dead Inside",
    "Disengagement",
    "No Mercy",
    "Instinct of the Master of Arms",
    "Armadillopack",
]

const weaponsAll = weaponsMelee.concat(weaponsRanged.concat(weaponsDualwield.concat(weaponsDOT)));
const mutationsAll = mutationsGeneric.concat(mutationsMelee.concat(mutationsRanged.concat(mutationsDOT)));

module.exports = {
    weaponsMelee, weaponsRanged, weaponsDualwield, weaponsDOT, weaponsAll, shields,
    skillsTurret, skillsGrenade, skillsPower,
    mutationsGeneric, mutationsMelee, mutationsRanged, mutationsDOT, mutationsAll, mutationsMisc
};