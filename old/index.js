const brutality = require('./brutality');
const tactics = require('./tactics');
const survival = require('./survival');

let slot1; //first slot
let slot2; //second slot
let skill1; //first skill
let skill2; //second skill
let mut1; //mutations
let mut2;
let mut3;

const stat = Math.floor((Math.random() * 3) + 1);
const sType =  Math.floor((Math.random() * 3) + 1);
const sType2 =  Math.floor((Math.random() * 3) + 1);

function pickRandom(x) {
  return x[Math.floor(Math.random() * x.length)];
}

let color;
if(stat == 1) color = brutality;
if(stat == 2) color = tactics;
if(stat == 3) color = survival;

//weapons + shields
slot1 = pickRandom(color.weaponsAll);
if(!color.weaponsDualwield.includes(slot1)) { slot2 = pickRandom(color.shields) }
else slot2 = 'None';

//skills
if(sType == 1) {skill1 = pickRandom(color.skillsTurret)};
if(sType == 2) {skill1 = pickRandom(color.skillsGrenade)};
if(sType == 3) {skill1 = pickRandom(color.skillsPower)};
if(sType2 == 1) {skill2 = pickRandom(color.skillsTurret)};
if(sType2 == 2) {skill2 = pickRandom(color.skillsGrenade)};
if(sType2 == 3) {skill2 = pickRandom(color.skillsPower)};

//mutations
mut1 = pickRandom(color.mutationsAll);
mut2 = pickRandom(color.mutationsMisc);
if(color.weaponsMelee.includes(slot1) && color.mutationsMelee.length > 0) mut3 = pickRandom(color.mutationsMelee);
else if(color.weaponsRanged.includes(slot1) && color.mutationsMelee.length > 0) mut3 = pickRandom(color.mutationsRanged);
else if(color.weaponsDOT.includes(slot1) && color.mutationsMelee.length > 0) mut3 = pickRandom(color.mutationsDOT);
else mut3 = pickRandom(color.mutationsAll);

console.log("-- Your Build Is --")
console.log(`Weapon: ${slot1}`);
console.log(`Shield: ${slot2}`);
console.log(`Skills: ${skill1} and ${skill2}`);
console.log(`Mutations: ${mut1}, ${mut2}, and ${mut3}`);

console.log("Debug Info: ", stat, sType, sType2);


