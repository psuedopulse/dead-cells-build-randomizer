const meleeMutations = [
  "Killer Instinct",
  "Melee",
  "Open Wounds",
  "Adrenaline",
  "Frenzy",
  "Scheme",
  "Initiative",
  "Predator",
  "Berserker",
]

const rangedMutations = [
  "Ranger's Gear",
  "Tranquility",
  "Point Blank",
  "Parting Gift",
  "Ripper",
  "Barbed Tips",
  "Networking",
  "Hunter's Instinct",
  "Ammo",
  "Acrobatipack",
]

const miscMutations = [
  "Combo",
  "Vengeance",
  "Tainted Flask",
  "Support",
  "Crow's Foot",
  "Tactical Retreat",
  "Soldier's Resistance",
  "Blind Faith",
  "Counterattack",
  "What Doesn't Kill Me",
  "Necromancy",
  "Extended Healing",
  "Gastronomy",
  "Spite",
  "Frostbite",
  "Heart of Ice",
  "Ygdar Orus Li Ox",
  "Recovery",
  "Emergency Triage",
  "Velocity",
  "Alienation",
  "Acceptance",
  "Masochist",
  "Dead Inside",
  "Disengagement",
  "No Mercy",
  "Instinct of the Master of Arms",
  "Armadillopack",
  "Porcupack",
]


module.exports = { meleeMutations, rangedMutations, miscMutations };
