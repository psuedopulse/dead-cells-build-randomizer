const brutalityMelee = [
  "Rusty Sword",
  "Balanced Blade",
  "Assassin's Dagger",
  "Spite Sword",
  "Blood Sword",
  "Twin Daggers",
  "Shovel",
  "Cursed Sword",
  "Sadist's Stiletto",
  "Swift Sword",
  "Giantkiller",
  "Shrapnel Axes",
  "Seismic Strike",
  "War Spear",
  "Impaler",
  "Rapier",
  "Meat Skewer",
  "Spartan Sandals",
  "Spiked Boots",
  "Hayabusa Gauntlets",
  "Valmont's Whip",
  "Wrenching Whip",
  "Oiled Sword",
  "Torch",
  "Frantic Sword",
  "Flawless",
  "Flint",
  "Tentacle",
  "Vorpan",
  "Flashing Fans",
  "Crowbar",
  "Snake Fangs",
  "Iron Staff",
  "Hattori's Katana",
  "Machete and Pistol",
  "Hard Light Sword",
  "Pure Nail",
  "Bone",
  "Abyssal Trident",
  "Hand Hook",
  "Bladed Tonfas",
  "Queen's Rapier"
];

const survivalMelee = [
  "Seismic Strike",  
  "Nutcracker",
  "Shovel",
  "Symmetrical Lance",
  "Flawless",
  "Flint",
  "Scythe Claws",
  "Iron Staff",
  "Tombstone",
  "Oven Axe",
  "Toothpick",
  "Maw of the Deep",
  "Wrecking Ball"
]

const tacticsMelee = [
  "Sadist's Stiletto",
  "Valmont's Whip",
  "Tentacle",
  "Flashing Fans",
  "Snake Fangs",
  "Machete and Pistol",
  "Hard Light Sword"
]

module.exports = { brutalityMelee, survivalMelee, tacticsMelee };

