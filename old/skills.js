const tacticsTurrets = [
  "Double Crossb-o-matic",
  "Sinew Slicer",
  "Heavy Turret",
  "Barnacle",
  "Flamethrower Turret",
  "Cleaver",
  "Wolf Trap",
  "Crusher",
  "Explosive Decoy",
  "Emergency Door",
  "Tesla Coil",
  "Scavenged Bombard",
]

const brutalityTurrets = [
  "Sinew Slicer",
  "Cleaver",
  "Flamethrower",
]

const survivalTurrets = [
  "Scavenged Bombard",
]

const brutalityGrenades = [
  "Powerful Grenade",
  "Infantry Grenade",
  "Cluster Grenade",
  "Fire Grenade",
  "Oil Grenade",
]

const tacticsGrenades = [
  "Magentic Grenade",
  "Oil Grenade",
  "Swarm",
]

const survivalGrenades = [
  "Powerful Grenade",
  "Cluster Grenade",
  "Stun Grenade",
  "Ice Grenade",
  "Root Grenade",
]

const brutalityPowers = [
  "Knife Dance",
  "Corrupted Power",
  "Vampirism",
  "Grappling Hook",
  "Phaser",
  "Corrosive Cloud",
  "Lacerating Aura",
  "Lightspeed",
  "Telluric Shock",
  "Collector's Syringe",
  "Smoke Bomb",
  "Serenade",
  "Face Flask",
  "Leghugger",
]

const tacticsPowers = [
  "Leghugger",
  "Pollo Power",
  "Cocoon",
  "Scarecrow Sickes",
  "Lightning Rods",
  "Smoke Bomb",
  "Lightspeed",
  "Great Owl of War",
  "Wings of the Crow",
  "Wave of Denial",
  "Corrosive Cloud",
  "Phaser",
  "Tornado",
  "Knife Dance",
  "Corrupted POwer",
]

const survivalPowers = [
  "Tonic",
  "Vampirism",
  "Telluric Shock",
  "Death Orb",
  "Giant Whistle",
  "Collector's Syringe",
  "Mushroom Boi!",
  "Ice Armor",
  "Scarecrow Sickles",
  "Cocoon",
]

module.exports = { brutalityTurrets, brutalityGrenades, brutalityPowers, tacticsTurrets, tacticsGrenades, tacticsPowers, survivalTurrets, survivalGrenades, survivalPowers };
