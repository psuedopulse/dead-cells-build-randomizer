const weaponsMelee = [
    "Rusty Sword",
    "Balanced Blade",
    "Assassin's Dagger",
    "Spite Sword",
    "Twin Daggers",
    "Shovel",
    "Cursed Sword",
    "Swift Sword",
    "Giantkiller",
    "Shrapnel Axes",
    "War Spear",
    "Impaler",
    "Rapier",
    "Meat Skewer",
    "Spartan Sandals",
    "Spiked Boots",
    "Hayabusa Gauntlets",
    "Valmont's Whip",
    "Wrenching Whip",
    "Oiled Sword",
    "Frantic Sword",
    "Flawless",
    "Flint",
    "Tentacle",
    "Vorpan",
    "Flashing Fans",
    "Crowbar",
    "Iron Staff",
    "Hattori's Katana",
    "Machete and Pistol",
    "Pure Nail",
    "Bone",
    "Abyssal Trident",
    "Hand Hook",
    "Bladed Tonfas",
    "Queen's Rapier"
];

const weaponsRanged = [
    
]

const weaponsDualwield = [
    "Hard Light Sword",
]

const weaponsDOT = [
    "Blood Sword",
    "Sadist's Stiletto",
    "Torch",
    "Snake Fangs",
]

const shields = [
    "Front Line Shield",
    "Bloodthirsty Shield",
    "Assault Shield",
]

const skillsTurret = [
    "Sinew Slicer",
    "Cleaver",
    "Flamethrower",
]

const skillsGrenade = [
    "Powerful Grenade",
    "Infantry Grenade",
    "Cluster Grenade",
    "Fire Grenade",
    "Oil Grenade",
]

const skillsPower = [
    "Knife Dance",
    "Corrupted Power",
    "Vampirism",
    "Grappling Hook",
    "Phaser",
    "Corrosive Cloud",
    "Lacerating Aura",
    "Lightspeed",
    "Telluric Shock",
    "Collector's Syringe",
    "Smoke Bomb",
    "Serenade",
    "Face Flask",
    "Leghugger",
]

const mutationsGeneric = [
    "Combo",
    "Vengeance",
    "Tainted Flask",
    "Porcupack",
    "Adrenaline",
    "Frenzy",
    "Scheme",
    "Initiative",
    "Predator",
    "Killer Instinct",
]

const mutationsMelee = [
    "Melee",
]

const mutationsRanged = [

]

const mutationsDOT = [
    "Open Wounds",
]

const mutationsMisc = [
    "Extended Healing",
    "Gastronomy",
    "Ygdar Orus Li Ox",
    "Recovery",
    "Emergency Triage",
    "Velocity",
    "Alienation",
    "Acceptance",
    "Masochist",
    "Dead Inside",
    "Disengagement",
    "No Mercy",
    "Instinct of the Master of Arms",
    "Armadillopack",
]

const weaponsAll = weaponsMelee.concat(weaponsRanged.concat(weaponsDualwield.concat(weaponsDOT)));
const mutationsAll = mutationsGeneric.concat(mutationsMelee.concat(mutationsRanged.concat(mutationsDOT)));

module.exports = {
    weaponsMelee, weaponsRanged, weaponsDualwield, weaponsDOT, weaponsAll, shields,
    skillsTurret, skillsGrenade, skillsPower,
    mutationsGeneric, mutationsMelee, mutationsRanged, mutationsDOT, mutationsAll, mutationsMisc
};