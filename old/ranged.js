const tacticsRanged = [
  "Beginner's Bow",
  "Multiple-Knocks Bow",
  "Bow and Endless Quiver",
  "Marksman's Bow",
  "Sonic Carbine",
  "Infantry Bow",
  "Quick Bow",
  "Ice Bow",
  "Heavy Crossbow",
  "Repeater Crossbow",
  "Ice Crossbow",
  "Explosive Crossbow",
  "Alchemic Carbine",
  "Boomerang",
  "Hemorrhage",
  "The Boy's Axe",
  "War Javelin",
  "Hokuto's Bow",
  "Nerves of Steel",
  "Throwing Knife",
  "Electric Whip",
  "Firebrands",
  "Ice Shards",
  "Pyrotechnics",
  "Lightning Bolt",
  "Fire Blast",
  "Frost Blast",
  "Blowgun",
  "Magic Missiles",
  "Barrel Launcher",
  "Killing Deck",
  "Gilded Yumi",
];

const brutalityRanged = [
  "Infantry Bow",
  "Hemorrhage",
  "Throwing Knife",
  "Firebrands",
  "Pyrotechnics",
  "Fire Blast",
];

const survivalRanged = [
  "Ice Bow",
  "Heavy Crossbow",
  "Repeater Crossbow",
  "Ice Crossbow",
  "Explosive Crossbow",
  "The Boy's Axe",
  "Ice Shards",
  "Frost Blast",
];

module.exports = { tacticsRanged, brutalityRanged, survivalRanged };

